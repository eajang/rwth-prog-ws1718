public class Literal {

    private final Typ type;
    private final double value;
    private final String name;


    // Konstant
    public Literal(double value) {
        //TODO
        this.type = Typ.VALUE;
        this.value = value;
        this.name = "";
    }

    // Variable
    public Literal(String name) {
        //TODO
        this.type = Typ.VAR;
        this.value = 0.;
        this.name = name;
    }

    public Literal(Literal toCopy) {
        //TODO
        this.type = toCopy.type;
        this.value = toCopy.value;
        this.name = toCopy.name;
    }

    public Typ getType() {
        return this.type;
    }

    public double getValue() {
        return this.value;
    }

    public String getName() {
        return this.name;
    }

    /**
     * @return true iff the literal is the constant 0.0
     */
    public boolean isZero() {

        if (type == Typ.VALUE && value == 0.0) {
            return true;
        }
        return false;
    }

    /**
     * @return String representation of the literal
     */
    public String toString() {
        switch (type) {
            case VALUE:
                return "(" + this.value + ")";
            case VAR:
                return name;
            default:
                return null;
        }
    }

    /**
     * @return 1 for variables and 0 for constants
     */
    public int getDegree() {
        switch (type) {
            case VALUE:
                return 0;
            case VAR:
                return 1;
            default:
                return -1;
        }
    }

    /**
     * Substitutes a variable by a constant
     * Returns a copy of the literal, if it is a constant
     * Returns a copy of the literal, if it is not the intended variable
     * @param toSubstitute name of the variable to substitute
     * @param value double value by which the variable is substituted
     * @return the resulting literal
     */
    public Literal substitute(String toSubstitute, double value) {
        switch(this.getType()){
            case VAR :
                if (toSubstitute.equals(this.name)) {
                    return new Literal(value);
                }
            default :
                return new Literal(this);
        }
    }

    /**
     * Substitutes a variable by a constant
     * Returns the double value of constant
     * Returns a default value on variables
     * @param defaultValue default value for substitution
     * @return the resulting double value
     */
    public Literal evaluate(double defaultValue) {
        switch(this.getType()){
            case VALUE :
                return this.getValue();
            default :
                return defaultValue;
        }
    }

    /**
     * Converts an input string to a literal
     *
     * @param input String representation of literal
     * @return the resulting literal
     */
    public static Literal parse(String input) {
        if (input == null || input.equals("")) {
            return new Literal(1.);
        }
        double value = 0.;
        String name = "";
        input = input.replaceAll("[()]", "");
        try {
            value = Double.parseDouble(input);
            return new Literal(value);
        } catch (NumberFormatException e) {
            name = input;
        }
        return new Literal(name);
    }

}
