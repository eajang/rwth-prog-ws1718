public class Polynomial {

    private final Monomial summand;

    private final Polynomial summands;

    public Polynomial(Monomial summand, Polynomial summands) {
        //TODO
        this.summand = summand;
        this.summands = summands;
    }

    public Polynomial(Polynomial poly) {
        //TODO
        this.summand = poly.summand;
        this.summands = poly.summands;
    }

    /**
     * The empty polynomial interpreted as 0, which is the default representation.
     */
    public static final Polynomial ZERO = new Polynomial(null, null);

    public Polynomial(Monomial toLift) {
        //TODO
        this.summand = toLift;
        this.summands = Polynomial.ZERO;
    }

    public Monomial getSummand() {
        return this.summand;
    }
    
    public Polynomial getSummands() {
        return this.summands;
    }

    
    /**
     *@return true iff a polynomial is the empty polynomial 0 or represents a sum of zeros */
    public boolean isZero (){
        if (this.getSummand() == null && this.getSummands() == null) {
            return true;
        }
        if (this.getSummand() == null) {
            return this.getSummands().isZero();
        }
        if (this.getSummands() == null) {
            return this.getSummand().isZero();
        }
        return this.getSummand().isZero() && this.getSummands().isZero();
    }

    /**
     * @return String representation of the polynomial
     */
    public String toString () {
        if (this.isZero()) {
            return "0";
        }
        if (this.getSummand() == null || this.getSummand().isZero()) {
            return this.getSummands.toString();
        }
        if (this.getSummands() == null || this.getSummands().isZero()) {
            return this.getSummands().toString();
        }
        return this.getSummand().toString() + "+" + this.getSummands().toString();
    }

    /**
     * The degree is the maximum of the degrees of the summands
     * @return the degree of the polynomial
     */
    public int getDegree() {
        if (this.isZero()) {
            return 0;
        }
        if (this.getSummand() == null) {
            return this.getSummands().getDegree();
        }
        if (this.getSummands() == null) {
            return this.getSummand().getDegree();
        }
        return Math.max(this.getSummand().getDegree(), this.getSummands.getDegree());
    }


    /**
     * Substitutes a variable by a double value
     * Calls substitution of the base literal
     * @param toSubstitute name of the variable to substitute
     * @param value double value by which the variable is substituted
     * @return Polynomial resulting from applying
     * the substitution to each occurence of variab
     */
    public Polynomial substitute(String toSubstitute, double value) {
        if (this.isZero()) {
            return ZERO;
        }
        if (this.getSummand() == null) {
            return this.getSummands().substitute(toSubstitute, value);
        }
        if (this.getSummands() == null) {
            return new Polynomial(this.getSummand().substitute(toSubstitute, value));
        }
        Monomial substitutedSummand = this.getSummand().substitute(toSubstitute , value);
        Polynomial substitutedSummands =this.getSummands().substitute(toSubstitute , value);
        return new Polynomial(substitutedSummand ,substitutedSummands);
    }

    /**
     * Evaluates a polynomial
     * Every occuring variable is substituted by the default value
     * Result equals usual exponentiation
     * @param defaultValue default value for substitution
     * @return sum of the values resulting from evaluating every summand
     */
    public double evaluate(double defaultValue) {
        if (this.isZero()) {
            return 0;
        }
        if (this.getSummand() == null) {
            return this.getSummands().evaluate(defaultValue);
        }
        if (this.getSummands() == null) {
            return this.getSummand().evaluate(defaultValue);
        }
        return this.getSummand().evaluate(defaultValue) + this.getSummands().evaluate(defaultValue);
    }

    /**
     * @param input String representation of polynomial
     * @return the resulting polynomial
     */
    public static Polynomial parse(String input) {
        if (input == null || input.equals("")) {
            return ZERO;
        }
        String[] splitted = input.split("\\+", 2);
        if (splitted.length == 1) {
            return new Polynomial(Monomial.parse(splitted[0]));
        }
        return new Polynomial(Monomial.parse(splitted[0]), parse(splitted[1]));
    }

    public static void main(String[] args) {
        Polynomial p;
        Polynomial q;
        String[] testValues = {"0", "(-3.1415)", "(-1)*x^3+(3.0)*x*y^2", "x+(-1)^5", "3^5+2^6+(3)*(2)*(5)*(4)", "x", "x^4", "x^2*y*z+2*x+(-3)", "x^2+2*x*y+y^2", "(0.0)*x^1000+(0.0)*x*y*z^100+(0.0)^7", "(0.0)*x^1+(0.0)^0"};
        int[] expectedDegrees = {0, 0, 3, 1, 0, 1, 4, 4, 2, 0, 0};
        int i = 0;
        for (String s : testValues) {
            System.out.println("----------------------------------------------------------------");
            System.out.println("Testing polynomial read from " + s + ".");
            p = parse(s);
            System.out.println(p);
            System.out.println("isZero?: " + p.isZero());
            System.out.println("degree: " + p.getDegree());
            System.out.println("degree as expected: " + (p.getDegree() == expectedDegrees[i]));
            i++;
            q = p.substitute("x", 1.);
            System.out.println("x substituted by 1: " + q);
            System.out.println("x substituted by 1, rest substituted by 0: " + q.evaluate(0.0));
        }
    }
}
