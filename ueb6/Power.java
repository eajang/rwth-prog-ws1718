public class Power {

    private final int exponent;
    private final Literal literal;

    public Power(int exponent, Literal literal) {
        //TODO
        if (exponent < 0) {
            exponent = 0;
        }
        this.exponent = exponent;
        this.literal = literal;
    }

    public Power(Literal literal) {
        //TODO
        this.exponent = 1;
        this.literal = literal;
    }

    public Power(Power p) {
        //TODO
        this.exponent = p.exponent;
        this.literal = p.literal;
    }
    
    public Literal getLiteral() {
        return this.literal;
    }
    
    public int getExponent() {
        return this.exponent;
    }

    /**
     * @return true iff the literal is null or
     * the literal is the constant 0.0 and the exponent is not 0.
     */
    public boolean isZero() {
        if (this.getLiteral() == null ||
                (this.getLiteral().isZero() && this.getExponent() != 0)) {
            return true;
        }
        return false;
    }

    
    /**
     * Gives a String representation of the power
     * Prints "1" for a zero exponent
     * @return String representation of the power
     */
    public String toString() {
        if (this.isZero()) {
            return "0";
        } else if (exponent == 0) {
            return "1";
        } else if (exponent == 1) {
            return this.getLiteral().toString();
        } else {
            return this.getLiteral().toString() + "^" + this.getExponent();
        }
    }

    /**
     * Computes the degree of a power
     * The degree is the exponent multiplied by the degree of the base
     * @return The degree of the power
     */
    public int getDegree() {
        if (this.isZero()) {
            return 0;
        }
        return this.getLiteral().getDegree() * this.getExponent();
    }


    
    /**
     * Substitutes a variable by a double value
     * Calls substitution of the base literal
     * @param toSubstitute name of the variable to substitute
     * @param value double value by which the variable is substituted
     * @return Power with substituted base
     */
    public Power substitute(String toSubstitute, double value) {
        if (this.getLiteral() == null) {
            return new Power(this);
        }
        return new Power(this.getExponent(), this.getLiteral().substitute(toSubstitute, value));
    }

    /**
     * Evaluates a power with a constant as base
     * If base is a variable it is substituted by the default value
     * Result equals usual exponentiation
     * @param defaultValue default value for substitution
     * @return the resulting double value
     */
    public double evaluate(double defaultValue) {
        if (this.isZero()) {
            return 0.0;
        }
        return Math.pow(this.getLiteral().evaluate(defaultValue).getValue(), this.getExponent());
    }

    /**
     * Converts an input string to a power
     * Input
     *
     * @param input String representation of literal ^ exponent
     * @return the resulting power
     */
    public static Power parse(String input) {
        if (input == null || input.equals("")) {
            return new Power(Literal.parse(""));
        }
        String[] splitted = input.split("\\^", 2);
        if (splitted.length == 1) {
            return new Power(Literal.parse(splitted[0]));
        }
        int exponent = 1;
        try {
            exponent = Integer.parseInt(splitted[1]);
        } catch (NumberFormatException e) {
            exponent = 1;
        }

        return new Power(exponent, Literal.parse(splitted[0]));

    }

}
