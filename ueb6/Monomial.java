public class Monomial {

    private final Power factor;

    private final Monomial factors;

    public Monomial(Power factor, Monomial factors) {
        this.factor = factor;
        this.factors = factors;

    }

    public Monomial(Monomial mon) {
        //TODO
        this.factor = mon.factor;
        this.factors = mon.factors;

    }

    /**
     * The empty monomial interpreted as 1
     */
    public static final Monomial ONE = new Monomial(null, null);

    public Monomial(Power toLift) {
        //TODO
        this.factor = toLift;
        this.factors = Monomial.ONE;
    }

    public Power getFactor() {
        return this.factor;
    }
    
    public Monomial getFactors() {
        return this.factors;
    }
    
    /**
     * @return true iff a monomial is the empty monomial 1
     */
    public boolean isOne() {
        return (this.getFactor() == null && this.getFactors() == null);
    }
    /**
     * @return true iff the monomial is zero, i.e., one factor is zero
     */
    public boolean isZero() {
        if (factor.isOne()) {
            return false;
        }
        if (this.getFactor() == null) {
            return this.getFactors.isZero();
        }
        if (this.getFactors() == null) {
            return this.getFactor().isZero();
        }
        return this.getFactor().isZero || this.getFactors.isZero();
    }

    /**
     * @return ein String repraesentiert ein Monom
     */
    public String toString() {
        if (this.isOne()) {
            return "1";
        }
        if (this.getFator() == null) {
            return this.getFactors().toString();
        }
        if (this.getFactors() == null) {
            return this.getFacto().toString();
        }
        if (this.getFactors().isOne()) {
            return (this.getFactor.toString());
        }
        return this.getFactor().toString() + "*" + this.getFactors().toString();
    }

    /**
     * The degree is the sum of the degrees of the factors
     * @return the degree of the monomial
     */
    public int getDegree() {
        if (this.isOne() || this.isZero()) {
            return 0;
        }
        if (this.getFactor() == null) {
            return this.getFactors().getDegree();
        }
        if (this.getFactors() == null) {
            return this.getFactor().getDegree();
        }

        return this.getFactor().getDegree() + this.getFactors().getDegree();
    }

    /**
     * Substitutes a variable by a double value
     * Calls substitution of the base literal
     * @param toSubstitute name of the variable to substitute
     * @param value double value by which the variable is substituted
     * @return Monomial resulting from applying the substitution to each occurence of the variable
     */
    public Monomial substitute(String toSubstitute, double value) {
        if (this.isOne()) {
            return ONE;
        }
        if (this.getFactor() == null) {
            return this.getFactors().substitute(toSubstitute, value);
        }
        if (this.getFactors() == null) {
            return new Monomial(this.getFactor().substitute(toSubstitute, value));
        }
        Power substitutedFactor = this.getFactor().substitute(toSubstitute, value);
        Monomial substitutedFactors = this.getFactors().substitute(toSubstitute, value);
        return new Monomial(substitutedFactor, substitutedFactors);
    }

    /**
     * Evaluates a monomial
     * Every occuring variable is substituted by the default
     * Result equals usual multiplication
     * @param defaultValue default value for substitution
     * @return product of the values resulting from evaluating every factor
     */
    public double evaluate(double defaultValue) {
        if (this.isOne()) {
            return 1.;
        }
        if(this.getFactor()==null) {
            return this.getFactors().evaluate(defaultValue);
        }
        if(this.getFactors()==null) {
            return this.getFactor().evaluate(defaultValue);
        }
        return this.getFactor().evaluate(defaultValue) * this.getFactors().evaluate(defaultValue);
    }

    /**
     * @param input String representation of monomial
     * @return the resulting monomial
     */
    public static Monomial parse(String input) {
        if (input == null || input.equals("")) {
            return ONE;
        }
        String[] splitted = input.split("\\*", 2);
        if (splitted.length == 1) {
            return new Monomial(Power.parse(splitted[0]));
        }
        return new Monomial(Power.parse(splitted[0]), parse(splitted[1]));
    }

}
