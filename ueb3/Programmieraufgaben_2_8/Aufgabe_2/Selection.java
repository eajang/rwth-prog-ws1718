/**
 * Programmierung WS17/18
 * Uebungsblatt 3 - Aufgabe 2
 * Klasse Selection für Selectionsort
 *
 * 383622 Eunae Jang
 * 321636 Dwayne Leenen
 */

public class Selection {

    public static void selection(int[] a) {
        int minIndex = 0;
        for (int i = 0; i < a.length-1; i++) {
            minIndex = i;
            for (int j = i+1; j < a.length; j++) {
                if (a[j] < a[minIndex]) {
                    minIndex = j;
                }
            }
            int tmp = a[i];
            a[i] = a[minIndex];
            a[minIndex] = tmp;
        }
    }
}
