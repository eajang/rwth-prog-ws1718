public class Main {
    
    public static void simuliere (Coffeemaker machine) {
        
        // zu Beginn fuellt alle Faecher des Kaffeevollautomaten machine maximal auf.
        machine.refill();
        
        while (true) {
            
            // Auswahl zum Heissgetraenk
            String selectedHotdrink = SimpleIO.getString("Was moechten Sie trinken?\nZur Auswahl stehen\n" + machine.toString());
            
            // Auswahl zur Groesse
            String selectedLarge = SimpleIO.getString("Gross? (J)a / (N)ein");
            boolean large = false;
            if (selectedLarge.equals("J")) {
                large = true;
            }
            
            // Ausgabe Heissgetraenke
            if (machine.getDrink(selectedHotdrink, large)) {
                SimpleIO.output("Hier ist Ihr " + selectedHotdrink, "Ihr Heissgetraenk");
            } else{
                // Die while-Schleife beendet, falls der Benutzer ein nicht verfuegbares Getraenk eingegeben hat.
                SimpleIO.output("Nicht genug "+ selectedHotdrink, "Ihr Heissgetraenk");
                break;
            }
        }
        
    }
    
    public static void main (String [] arguments) {
        
        Coffeemaker bean_to_cup  = new Coffeemaker();
        
        bean_to_cup.maxwater = 2.;
        
        bean_to_cup.maxmilk = 0.75;
        
        bean_to_cup.maxbeans = 1.0;
        
        bean_to_cup.sorts = new Hotdrink[3];
        
        String[] names = {"Latte Macchiato", "Milchkaffee", "Espresso"};
        
        double [] waterArray = {0.2, 0.2, 0.05};
        
        double [] milkArray = {0.3, 0.1, 0.0};
        
        double [] beansArray = {0.04, 0.05, 0.01};
        
        for(int i = 0; i < 3; i++){
            
            bean_to_cup.sorts[i] = new Hotdrink();
            
            bean_to_cup.sorts[i].name = names[i];
            
            bean_to_cup.sorts[i].water = waterArray[i];
            
            bean_to_cup.sorts[i].milk = milkArray[i];
            
            bean_to_cup.sorts[i].beans = beansArray[i];
            
        }
        
        bean_to_cup.refill();
        
        simuliere( bean_to_cup );
        
    }
    
}

