
public class Coffeemaker {
    
    double maxwater;
    double maxmilk;
    double maxbeans;
    
    Hotdrink[] sorts;
    
    double currentWater;
    double currentMilk;
    double currentBeans;
    
    public void refill() {
        currentWater = maxwater;
        currentMilk = maxmilk;
        currentBeans = maxbeans;
    }
    
    public boolean getDrink(String hotdrink, boolean large) {
        
        boolean result = false;
        Hotdrink servingHotdrink = null;
        
        for (Hotdrink obj: sorts) {
            if (obj.name.equals(hotdrink)) {
                // dieses Heissgetraenk steht zur Verfuegung im Automat
                result = true;
                servingHotdrink = obj;
            }
        }
        
        if (servingHotdrink != null) {
            double neededWater = servingHotdrink.water;
            double neededMilk = servingHotdrink.milk;
            double neededBeans = servingHotdrink.beans;
            
            if (large) {                // gross
                neededWater *= 2;
                neededMilk *= 2;
                neededBeans *= 2;
            }
            
            if (currentWater < neededWater || currentMilk < neededMilk || currentBeans < neededBeans) {
                // nicht genuegend Zutaten enthalten
                result = false;
            } else {
                // genuegend Zutaten enthalten
                // aktualisiert die Fuellstaende der verschiedenen Faecher
                currentWater -= neededWater;
                currentMilk -= neededMilk;
                currentBeans -= neededBeans;
            }
        }
        
        return result;
    }
    
    /**
     * @return die aktuelle verfuegbare Groesse der Heissgetraenke und Namen
     */
    public String toString() {
        
        String actualDrinkInfo = "";
        
        for (Hotdrink hotdrink: sorts) {
            String largeInfo = "";
            
            double grossWater = hotdrink.water * 2;
            double grossMilk = hotdrink.milk * 2;
            double grossBeans = hotdrink.beans * 2;
            
            // die Mengen der Zutaten ueberpruefen
            if (currentWater >= grossWater && currentMilk >= grossMilk && currentBeans >= grossBeans) {
                largeInfo = "Gross/Normal";
                actualDrinkInfo += hotdrink.toString() + " " + largeInfo + "\n";
            } else if ((hotdrink.water <= currentWater && currentWater < grossWater)
                       || (hotdrink.milk <= currentMilk && currentMilk < grossMilk)
                       || (hotdrink.beans <= currentBeans && currentBeans < grossBeans)) {
                largeInfo = "nur Normal";
                actualDrinkInfo += hotdrink.toString() + " " + largeInfo + "\n";
            }
        }
        
        return actualDrinkInfo;
    }
    
}

