import java.util.Random;
import java.util.Arrays;

public class InsertionSort {
  public static int[] sortR(int[] arr) {
    // TODO
      if (arr.length <= 0) {
          return arr;
      } else {
          return sortRHelper(arr, 1);
      }
  }

  // Hilfsmethoden hier einfuegen.
    private static int[] sortRHelper(int[] arr, int upto) {
      if (upto >= arr.length) {
          return arr;
      } else {
          return sortRHelper(sortRHelper2(arr, upto), upto+1);
      }
    }

    private static int[] sortRHelper2(int[] arr, int current) {
      if (current <= 0) {
          return arr;
      } else {
          if (arr[current] < arr[current-1]) {
              int tmp = arr[current];
              arr[current] = arr[current-1];
              arr[current-1] = tmp;
          }
          return sortRHelper2(arr, current-1);
      }
    }


  public static int[] sortI(int[] arr) {
    for(int upto = 1; upto < arr.length; ++upto) {
      for(int current = upto; current > 0; --current) {
        if(arr[current] >= arr[current-1]) {
          break;
        } else {
          //aktuelles Element gehoert weiter vorne einsortiert
          int tmp = arr[current];
          arr[current] = arr[current-1];
          arr[current-1] = tmp;
        }
      }
    }
    return arr;
  }

  public static void main(String[] args) {
    Random rng = new Random();
    boolean passed = true;

    passed &= test(new int[0]);
    {
      int[] arr = new int[1];
      arr[0] = rng.nextInt(20) - 7;
      passed &= test(arr);
    }
    for(int i = 0; i < 10; ++i) {
      int[] arr = new int[rng.nextInt(20)];
      for(int j = 0; j < arr.length; ++j) {
        arr[j] = rng.nextInt(20) - 7;
      }
      passed &= test(arr);
    }

    if(passed) {
      System.out.println("All tests passed.");
    } else {
      System.out.println("Some tests failed!");
    }
  }

  private static boolean test(int[] arr) {
    int[] resA = sortI(Arrays.copyOf(arr, arr.length));
    int[] resB = sortR(Arrays.copyOf(arr, arr.length));

    System.out.println(Arrays.toString(resA));
    System.out.println(Arrays.toString(resB));

    return Arrays.equals(resA, resB);
  }
}
