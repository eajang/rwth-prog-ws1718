/**
 * Diese Klasse implementiert eine Aufzugsteuerung. Der Aufzug kann zu
 * bestimmten Stockwerken gerufen werden und mit der Methode aufzugStarten()
 * wird jeweils immer ein neues Stockwerk abgearbeitet, sofern mindestens eines
 * angefragt ist.
 */
public class AufzugSteuerung {
  private final boolean[] stockwerkAngefragt;

  private AufzugZustand zustand;

  private final Aufzug aufzug;

  /**
   * Erzeugt eine Steuerung zu einem gegebenen Aufzug.
   * @param aufzug der zu steuernde Aufzug
   */
  public AufzugSteuerung(Aufzug aufzug) {
      this.aufzug = aufzug;
      aufzug.tuerOeffnen();
      stockwerkAngefragt = new boolean[Math.abs(aufzug.getMaxStockwerk() - aufzug.getMinStockwerk()) + 1];
      zustand = AufzugZustand.Warten;
  }

  /**
   * Gibt das aktuelle Stockwerk, normalisiert als Array-Index, zurueck.
   * @return Index des aktuellen Stockwerks
   */
  private int aktuellesStockwerk() {
      return aufzug.getAktuellesStockwerk() - aufzug.getMinStockwerk();
  }

  /**
   * Faehrt den Aufzug zu einem gegebenen Stockwerk.
   * @param stockwerk Der Array-Index des Stockwerks
   */
  private void fahren(int stockwerk) {
      aufzug.tuerSchliessen();
      aufzug.fahren(stockwerk + aufzug.getMinStockwerk());
      aufzug.tuerOeffnen();
  }

  /**
   * Ruft den Aufzug zu einem gegebenen Stockwerk. Das Stockwerk
   * wird intern in einen Array-Index umgerechnet. Das aktuelle
   * Stockwerk kann nicht angefragt werden, der Aufzug ist ja schon da.
   * @param stockwerk Die Nummer des Stockwerks
   */
  public void rufen(int stockwerk) {
      if(aufzug.getAktuellesStockwerk() != stockwerk
              && stockwerk <= aufzug.getMaxStockwerk()
              && stockwerk >= aufzug.getMinStockwerk()) {
          this.stockwerkAngefragt[stockwerk - aufzug.getMinStockwerk()] = true;
    }
  }

  /**
   * Arbeitet ein Stockwerk ab, sofern mindestens ein Stockwerk
   * angefragt ist.
   */
  public void aufzugStarten() {

      int aktuell = aktuellesStockwerk();
      Integer naechstesStockwerk = null;
      switch(zustand) {
          case Warten:
              naechstesStockwerk = sucheNaechstesStockwerk(aktuell);
              if (naechstesStockwerk == null) {
                  return;
              }

              if (naechstesStockwerk > aktuell) {
                  zustand = AufzugZustand.Hoch;
              } else if (naechstesStockwerk < aktuell) {
                  zustand = AufzugZustand.Runter;
              }
              break;

          case Hoch:
              naechstesStockwerk = sucheNaechstesHoeherStockwerk(aktuell);
              if (naechstesStockwerk == null) {
                  naechstesStockwerk = sucheNaechstesUntererStockwerk(aktuell);
                  if (naechstesStockwerk == null) {
                      zustand = AufzugZustand.Warten;
                      return;
                  } else {
                      zustand = AufzugZustand.Runter;
                  }
              } else {
                  zustand = AufzugZustand.Hoch;
              }
              break;

          case Runter:
              naechstesStockwerk = sucheNaechstesUntererStockwerk(aktuell);
              if (naechstesStockwerk == null) {
                  naechstesStockwerk = sucheNaechstesHoeherStockwerk(aktuell);
                  if (naechstesStockwerk == null) {
                      zustand = AufzugZustand.Warten;
                      return;
                  } else {
                      zustand = AufzugZustand.Hoch;
                  }
              } else {
                  zustand = AufzugZustand.Runter;
              }
              break;
      }
      fahren(naechstesStockwerk);
      stockwerkAngefragt[naechstesStockwerk] = false;
  }

  /**
   * Rechne die Distanz zwischen zwei ganzen Zahlen
   * @param a eine ganze Zahl
   * @param b eine ganze Zahl
   * @return (der Betrag der) distanz der zwei ganzen Zahlen
   * */
  private int distanz (int a, int b) {
      return Math.abs(a-b);
  }

  /**
   * Suche das naechstgelegene andere Stockwerk, das angefragt ist
   * @param aktuell ein aktuelles Stockwerk
   * @return das naechstgelegene andere Stockwerk, das angefragt ist
   */
  private Integer sucheNaechstesStockwerk(int aktuell) {

      Integer naechstesHoeher = sucheNaechstesHoeherStockwerk(aktuell);
      Integer naechstesUnterer = sucheNaechstesUntererStockwerk(aktuell);

      if (naechstesHoeher == null) {
          if (naechstesUnterer == null) {
              return null;
          }
          return naechstesUnterer;
      } else if (naechstesUnterer == null) {
          return naechstesHoeher;
      } else if (distanz(aktuell, naechstesHoeher)
              > distanz(aktuell, naechstesUnterer)) {
          return naechstesUnterer;
      } else {
          return naechstesHoeher;
      }
  }

  /**
   * Suche das naechste angefragte Stockwerk ueber dem aktuellen Stockwerk
   * @param aktuell ein aktuelles Stockwerk
   * @return das naechste angefragte Stockwerk ueber dem aktuellen Stockwerk
   */
  private Integer sucheNaechstesHoeherStockwerk(int aktuell) {
      for (int i = aktuell + 1; i < stockwerkAngefragt.length; i++) {
          if (stockwerkAngefragt[i]) {
              return i;
          }
      }
      return null;
  }

  /**
   * Suche das naechste angefragte Stockwerk unter dem aktuellen Stockwerk
   * @param aktuell ein aktuelles Stockwerk
   * @return das naechste angefragte Stockwerk unter dem aktuellen Stockwerk
   */
  private Integer sucheNaechstesUntererStockwerk(int aktuell) {
      for (int i = aktuell - 1; i >= 0; i--) {
          if (stockwerkAngefragt[i]) {
              return i;
          }
      }
      return null;
  }

}
