/**
 * Objekte dieser Klasse repraesentieren einen Aufzug
 *
 * @author (383622) Eunae Jang
 * @author (321636) Dwayne Leenen
 */
public class Aufzug {

    /**
     * Diese ganze Zahl repraesentiert das aktuelles Stockwerk
     */
    private int aktuellesStockwerk;

    /**
     * Diese ganze Zahl repraesentiert die Anzahl der Personen, die aktuell im Aufzug sind
     */
    private int anzahlPersonen;

    /**
     * Diese boolesche Variable repraesentiert, ob die Tuer des Aufzugs geoeffnet ist
     * true: die Tuer auf, false: die Tuer zu
     */
    private boolean zustandTuer;

    /**
     * Diese ganze Zahl repraesentiert ein maximales Stockwerk
     * nie aendern: final (-1)
     */
    private final int maxStockwerk;

    /**
     * Diese ganze Zahl repraesentiert ein minimales Stockwerk
     * nie aendern: final (-1)
     */
    private final int minStockwerk;

    /**
     * Diese ganze Zahl repraesentiert eine maximale Anzahl an Passagieren
     * nie aendern: final (-1)
     */
    private final int maxPersonen;

    /**
     * Diese ganze Zahl repraesentiert eine eigene ID eines Aufzugs.
     * Diese ID beginnen bei 0.
     */
    private final int ID;

    /**
     * gibt an, welche ID die naechste Objekt der Klasse habt
     */
    private static int nextID = 0;

    /**
     * Erstelle einen neuen Aufzug mit einem bestimmten maximalen, minimalen Stockwerk
     * und einer bestimmten maximale Anzahl an Passagieren.
     *
     * @param maxStockwerk ein maximales Stockwerk
     * @param minStockwerk ein minimales Stockwerk
     * @param maxPersonen eine maximale Anzahl an Passagieren
     */
    public Aufzug (int maxStockwerk, int minStockwerk, int maxPersonen) {
        this.maxStockwerk = maxStockwerk;
        this.minStockwerk = minStockwerk;
        this.maxPersonen = maxPersonen;


        this.ID = nextID;
        nextID++;

        this.zustandTuer = false;
        this.aktuellesStockwerk = this.minStockwerk;
        this.anzahlPersonen = 0;
    }

    /**
     * Erstelle einen neuen Aufzug mit einem bestimmten maximalen
     * und einer bestimmten maximalen Anzahl an Passagieren.
     * Eine minimale Stockwerk wird in diesem Fall bei 0 initialisiert.
     *
     * @param maxStockwerk ein maximales Stockwerk
     * @param maxPersonen eine maximale Anzahl an Passagieren
     */
    public Aufzug (int maxStockwerk, int maxPersonen) {
        this.maxStockwerk = maxStockwerk;
        this.minStockwerk = 0;
        this.maxPersonen = maxPersonen;

        this.ID = nextID;
        nextID++;

        this.zustandTuer = false;
        this.aktuellesStockwerk = this.minStockwerk;
        this.anzahlPersonen = 0;
    }

    /**
     * Lese das aktuelle Stockwerk
     *
     * @return das aktuelle Stockwerk
     */
    public int getAktuellesStockwerk() {
        return this.aktuellesStockwerk;
    }

    /**
     * Lese die aktuelle Anzahl der Personen, die im Aufzug sind.
     *
     * @return die aktuelle Anzahl der Personen, die im Aufzug sind.
     */
    public int getAnzahlPersonen() {
        return this.anzahlPersonen;
    }

    /**
     * Prueft, ob die Tuer des Aufzugs geoeffnet ist.
     *
     * @return wahr wenn die Tuer geoeffnet ist.
     */
    public boolean isTuerAuf() {
        return this.zustandTuer;
    }

    /**
     * Lese das maximale Stockwerk des Aufzugs
     *
     * @return das maximale Stockwerk des Aufzugs
     */
    public int getMaxStockwerk() {
        return this.maxStockwerk;
    }

    /**
     * Lese das minimale Stockwerk des Aufzugs
     *
     * @return das minimale Stockwerk des Aufzugs
     */
    public int getMinStockwerk() {
        return this.minStockwerk;
    }

    /**
     * Lese die maximale Anzahl an Passagieren des Aufzugs
     *
     * @return die maximale Anzahl an Passagieren des Aufzugs
     */
    public int getMaxPersonen() {
        return this.maxPersonen;
    }

    /**
     * Lese die ID des Aufzugs
     *
     * @return ID des Aufzugs
     */
    public int getID() {
        return this.ID;
    }

    /**
     * Setze, die Tueren zu oeffnen
     */
    public void tuerOeffnen() {
        this.zustandTuer = true;
    }

    /**
     * Setze, die Tueren zu schliessen
     */
    public void tuerSchliessen() {
        this.zustandTuer = false;
    }


    /**
     * Die eingegebenen Personen steigen ein,
     * weshalb die Anzahl der Passagiere aendert sich.
     *
     * @param anzahlEinPersonen die Anzahl der Personen, die einsteigen moechten
     * @return die Anzahl der Personen, die nicht einsteigen konnten
     */
    public int einsteigen(int anzahlEinPersonen) {
        if (anzahlEinPersonen < 0 || !this.zustandTuer) {
            return anzahlEinPersonen;
        }

        int freierPlatz = this.getMaxPersonen() - this.anzahlPersonen;
        if (anzahlEinPersonen <= freierPlatz) {
            this.anzahlPersonen += anzahlEinPersonen;
        } else if (anzahlEinPersonen > freierPlatz) {
            this.anzahlPersonen = this.getMaxPersonen();
        }
        return ((anzahlEinPersonen - freierPlatz) < 0 ? 0 : (anzahlEinPersonen - freierPlatz));
    }

    /**
     * Die eingegebenen Personen steigen aus,
     * weshalb die Anzahl der Passagiere aendert sich.
     *
     * @param anzahlAusPersonen die Anzahl der Personen, die aussteigen moechten
     */
    public void aussteigen(int anzahlAusPersonen) {
        if (anzahlAusPersonen < 0) {
            return;
        }
        if (this.zustandTuer) {
            this.anzahlPersonen -= anzahlAusPersonen;

            if (this.anzahlPersonen < 0) {
                this.anzahlPersonen = 0;
            }
        }
    }

    /**
     * ermoeglicht zu einem gegebenen Stockwerk zu fahren
     *
     * @param zielStockwerk das Stockwerk, in das der Aufzug faehrt.
     * @return true, wenn der Aufzug tatsaechlich gefahren ist.
     */
    public boolean fahren(int zielStockwerk) {
        if (this.zustandTuer) {
            return false;
        }

        if (zielStockwerk < this.getMinStockwerk()) {
            this.aktuellesStockwerk = this.getMinStockwerk();
        } else if (zielStockwerk > this.getMaxStockwerk()) {
            this.aktuellesStockwerk = this.getMaxStockwerk();
        } else {
            this.aktuellesStockwerk = zielStockwerk;
        }

        return true;
    }
}
