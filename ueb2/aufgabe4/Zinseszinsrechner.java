
/**
 * Programmierung WS17/18
 * Uebungsblatt 2 - Aufgabe 4
 * Zinseszinsrechner
 *
 * 383622 Eunae Jang
 * 321636 Dwayne Leenen
 */
public class Zinseszinsrechner
{
    public static void main(String[] args){
        
        // Fragen nach dem Startbetrag und dem Zinssatz in Prozent
        double startBetrag  = 0.0;
        double zinssatz     = 0.0;
        
        while(startBetrag == 0.0) {
            startBetrag = SimpleIO.getDouble("Bitte geben Sie den Startbetrag ein.");
        }
        
        while(zinssatz == 0.0) {
            zinssatz = SimpleIO.getDouble("Bitte geben Sie den Zinssatz als Prozentwert ein.");
        }
        
        // Fragen, was der Benutzer berechnen moechte.
        // Dabei Stehen ein Ziel oder die Zeit zur Auswahl.
        String auswahl = "";
        auswahl = SimpleIO.getString("Bitte Waehlen Sie aus: \n Ziel: Berechnet die Zeit, bis ein gegebener Betrag angespart wurde.\n Zeit: Berechnet den Betrag, der nach einer gegebenen Zeit angespart wurde.");
        
        
        if (auswahl.equals("Ziel")) {                   // bei der Auswahl Ziel
            
            double zielBetrag = 0.0;
            while( zielBetrag == 0.0 ) {
                zielBetrag = SimpleIO.getDouble("Bitte geben Sie den Zielbetrag ein.");
            }
            
            double sparBetrag = startBetrag;
            int jahre = 0;
            while (sparBetrag < zielBetrag) {
                jahre++;
               
                sparBetrag += (zinssatz * 0.01 * sparBetrag);
            }
            
            SimpleIO.output("Es dauert "+jahre+" Jahr(e) bei einem Zinssatz von "+zinssatz+"%, um von "+startBetrag+" auf den Betrag "+zielBetrag+" zu sparen. Nach dieser Zeit hat man "+ sparBetrag+".", "Resultat");
        
        } else if (auswahl.equals("Zeit")) {            // bei der Auswahl Zeit
            
            int jahre = 0;
            while( jahre == 0 ) {
                jahre = SimpleIO.getInt("Bitte geben Sie eine Anzahl von Jahren ein.");
            }
            
            double sparBetrag = startBetrag;
            for (int i = 0; i < jahre; i++) {
                sparBetrag += (zinssatz * 0.01 * sparBetrag);
            }
            
            SimpleIO.output("Bei einem Zinssatz von "+zinssatz+"%, und einem Startbetrag von "+startBetrag+" hat man nach "+jahre+" Jahr(en) "+ sparBetrag+" gespart.", "Resultat");
            
        } else {                                        // anzeigen Fehlermeldung und sich beenden
            SimpleIO.output("Geben Sie keines von beidem ein. Beendet dieses Programm.", "Fehler");
        }
    }
    
}
