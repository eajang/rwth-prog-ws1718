package weihnachtsmarkt;

import io.SimpleIO;
import weihnachtsmarkt.staende.*;
import zufall.Zufall;

public class Weihnachtsmarkt {
    private Stand[] staende;

    public Weihnachtsmarkt(int anzahl) {
        this.staende = new Stand[anzahl];

        for (int i = 0; i < anzahl; i++) {
            String name = Zufall.name();
            Stand s;

            switch(Zufall.zahl(4)) {
                case 0: {
                    s = new Weihnachtsartikelstand(name, Zufall.zahl(21));
                    break;
                }
                case 1: {
                    s = new Suesswarenstand(name, Zufall.zahl(3), Zufall.suessware());
                    break;
                }
                case 2: {
                    s = new Gluehweinstand(name, Zufall.zahl(3));
                    break;
                }
                case 3: default: {
                    s = new Flammkuchenstand(name, Zufall.zahl(3));
                    break;
                }
            }
            staende[i] = s;
        }
    }

    public static void main(String args[]) {
        Weihnachtsmarkt weihnachtsmarkt = new Weihnachtsmarkt(5);

        boolean verlassen = false;

        while (!verlassen) {
            System.out.println("Der Weihnachtsmarkt besteht aus folgenden Staeden:" + System.lineSeparator());
            for(int j = 0; j < 5; j++) {
                Stand stand = weihnachtsmarkt.staende[j];
                System.out.println(j + ": " + stand + System.lineSeparator());
            }

            int auswahlStand = SimpleIO.getInt("Welchen Stand mochten Sie Besuchen?");
            weihnachtsmarkt.staende[auswahlStand].verkaufe();


            for(int i = 0; i < weihnachtsmarkt.staende.length; i++) {
                if (weihnachtsmarkt.staende[i] instanceof Weihnachtsartikelstand) {
                    Weihnachtsartikelstand stand = (Weihnachtsartikelstand)weihnachtsmarkt.staende[i];
                    if (stand.getBesucherProStunde() < 30) {
                        stand.verschiebe(i);
                    }
                } else if (weihnachtsmarkt.staende[i] instanceof Suesswarenstand) {
                    Suesswarenstand stand = (Suesswarenstand)weihnachtsmarkt.staende[i];
                    if (stand.getBesucherProStunde() < 30) {
                        stand.verschiebe(i);
                    }
                }
            }

            verlassen = SimpleIO.getBoolean("Moechten Sie den Weihnachtsmarkt verlassen?");
            System.out.println(verlassen);
        }

    }
}