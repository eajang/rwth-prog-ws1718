package weihnachtsmarkt.staende;

public class Gluehweinstand extends Lebensmittelstand {

    public Gluehweinstand(String name, double preis) {
        super(name, preis);
    }

    public String toString() {
        return "Gluehweinstand: " + System.lineSeparator() + super.toString();
    }
}