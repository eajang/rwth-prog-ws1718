package weihnachtsmarkt.staende;

public class Suesswarenstand extends Lebensmittelstand implements Verschiebbar{

    private String art;

    public Suesswarenstand(String name, int preis, String art) {
        super(name, preis);
        this.art = art;
    }

    public String getArt() {
        return art;
    }

    public void setArt(String art) {
        this.art = art;
    }

    public String toString() {
        return "Suesswarenstand (" + art + "):" + System.lineSeparator() + super.toString();
    }

    public void verschiebe(int i) {
        setBesucherProStunde(berechneBesucherProStunde());
        System.out.println("Stand " + i + " wurde verschoben und wird jetzt von " + getBesucherProStunde() + " Passanten pro Stunde besucht.");
    }

}