package weihnachtsmarkt.staende;

import weihnachtsmarkt.*;
import zufall.Zufall;

public class Stand {

    private String verkaeuferName;
    private int besucherProStunde;

    public Stand(String name) {
        this.verkaeuferName = name;
        this.besucherProStunde = Zufall.zahl(101);
    }

    public String getVerkaeuferName() {
        return verkaeuferName;
    }

    public void setVerkaeuferName(String name) {
        this.verkaeuferName = name;
    }

    public int getBesucherProStunde() {
        return besucherProStunde;
    }

    public void setBesucherProStunde(int besucher) {
        this.besucherProStunde = besucher;
    }

    public int berechneBesucherProStunde() {
        return Zufall.zahl(101);
    }

    public void verkaufe() {
        System.out.println("Guten Tag!");
    }

    public String toString() {
        return "Verkauefer: " + this.getVerkaeuferName() + System.lineSeparator()
                + "Besucher pro Stunde: " + this.getBesucherProStunde();
    }
}