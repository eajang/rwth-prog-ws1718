package weihnachtsmarkt.staende;

public class Flammkuchenstand extends Lebensmittelstand {

    public Flammkuchenstand(String name, double preis) {
        super(name, preis);
    }

    public String toString() {
        return "Flammkuchenstand: " + System.lineSeparator() + super.toString();
    }
}