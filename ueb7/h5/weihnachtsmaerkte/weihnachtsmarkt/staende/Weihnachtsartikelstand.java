package weihnachtsmarkt.staende;

import io.SimpleIO;
import zufall.Zufall;
import weihnachtsmarkt.artikel.*;


public class Weihnachtsartikelstand extends Stand implements Verschiebbar{
    private Artikel[] arrArtikel;

    public Weihnachtsartikelstand(String name, int anzahlArtikel) {
        super(name);

        if (anzahlArtikel <= 0) {
            anzahlArtikel = 1;
        }

        this.arrArtikel = new Artikel[anzahlArtikel];
        for(int i = 0; i < anzahlArtikel; i++) {
            this.arrArtikel[i] = new Artikel(Zufall.artikel(), Zufall.zahl(10));
        }

        this.setBesucherProStunde(anzahlArtikel+Zufall.zahl(6));

    }

    public Artikel[] getArrArtikel() {
        return arrArtikel;
    }

    public void setArrArtikel(Artikel[] arrArtikel) {
        this.arrArtikel = arrArtikel;
    }

    public int berechneBesucherProStunde() {
        return this.arrArtikel.length + Zufall.zahl(6);
    }

    public void verkaufe() {
        super.verkaufe();

        boolean istFortsetzt = true;
        double gesamtpreis = 0.;

        while(istFortsetzt) {
            System.out.println("Unsere Artikel sind:");
            for (int i = 0; i < this.getArrArtikel().length; i++) {
                Artikel artikel = this.getArrArtikel()[i];
                System.out.println(i + ":" + artikel.getName() + " (" + artikel.getPreis()+" Euro)" );
            }

            int auswahl = SimpleIO.getInt("Welchen Artikel moechten Sie kaufen?");

            Artikel ausgewaehlterArtikel = this.getArrArtikel()[auswahl];
            System.out.println(ausgewaehlterArtikel.getName()
                    + " wird eingepackt. Viel Spass damit!");

            istFortsetzt = SimpleIO.getBoolean("Darf es sonst noch etwas sein?");

            gesamtpreis += ausgewaehlterArtikel.getPreis();
        }
        System.out.println(gesamtpreis + " Euro, bitte. ");
    }

    public String toString() {
        return "Weihnachtsartikelstand: " + System.lineSeparator() + super.toString();
    }

    public void verschiebe(int i) {
        setBesucherProStunde(berechneBesucherProStunde());
        System.out.println("Stand " + i + " wurde verschoben und wird jetzt von " + getBesucherProStunde() + " Passanten pro Stunde besucht.");

    }
}