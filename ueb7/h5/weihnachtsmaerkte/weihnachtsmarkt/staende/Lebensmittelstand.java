package weihnachtsmarkt.staende;

import io.SimpleIO;

public class Lebensmittelstand extends Stand {
    private double preisProHundertGramm;

    public Lebensmittelstand(String name, double preis) {
        super(name);

        if (preis <= 0.) {
            preis = 0.01;
        }
        this.preisProHundertGramm = preis;
    }

    public double getPreisProHundertGramm() {
        return preisProHundertGramm;
    }

    public void setPreisProHundertGramm(double preisProHundertGramm) {
        this.preisProHundertGramm = preisProHundertGramm;
    }

    public void verkaufe() {
        boolean istFortsetzt = true;
        double gesamtpreis = 0.;

        while(istFortsetzt) {
            int gramm = SimpleIO.getInt("Wie viel Gramm moechten Sie?");
            System.out.println(gramm + " Gramm fuer Sie. Lassen Sie es sich schmecken!");
            istFortsetzt = SimpleIO.getBoolean("Darf es sonst noch etwas sein?");
            gesamtpreis += (preisProHundertGramm * gramm) / 100.;
        }
        System.out.println(gesamtpreis + " Euro, bitte. ");
    }

    public String toString() {
        return "Preis pro 100g: " + this.getPreisProHundertGramm() + "Euro"
                + System.lineSeparator() + super.toString();
    }
}