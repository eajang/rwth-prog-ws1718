package weihnachtsmarkt.artikel;

public class Artikel {
    private String name;
    private double preis;

    public Artikel (String name, double preis) {
        this.name = name;
        if (preis <= 0.0) {
            preis = 0.01;
        }
        this.preis = preis;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPreis() {
        return preis;
    }

    public void setPreis(double preis) {
        if (preis <= 0.0) {
            preis = 0.01;
        }
        this.preis = preis;
    }
}
