public class A extends Object{
    public static int x = 0;
    public int y = 7;

    public A () { // Signatur : A()
        this (x );
        x ++;
        System.out.println("A()");
    }
    public A (int x) { // Signatur : A(I)
        x = x + 2;
        y = y - x ;
        System.out.println("A(I)");
    }

    public A (double x) { // Signatur : A(D)
         y += x ;

        System.out.println("A(D)");
    }

    public void f ( int i , A o ) {

        System.out.println("A.f(IA)");
    }

    public void f ( Long lo , A o) {

        System.out.println("A.f(LA)");
    }

    public void f ( double d , A o) {
        System.out.println("A.f(DA)");
    }
}