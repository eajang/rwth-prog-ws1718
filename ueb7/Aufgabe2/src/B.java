public class B extends A{
    public float x = 1.5f ;
    public int y = 1;

     public B () { // Signatur : B()
          x ++;
         System.out.println("B()");
     }

    public B(float x) { // Signatur : B(F)
        super(x);
        super.y ++;
        System.out.println("B(F)");
    }

    public void f ( int i , B o ) {
        System.out.println("B.f(IB)");
    } // Signatur : B.f(IB)
    public void f ( int i , A o ) {
        System.out.println("B.f(IA)");
    } // Signatur : B.f(IA)

    public void f ( long lo , A o) {
        System.out.println("B.f(LA)");
    }
}
