public class Z {
    public static void main(String [] args) {

        X xx1 = new X(42);
        System.out.println("X.a: "+xx1.a);
        System.out.println("-----------------------");
        X xx2 = new X(22.99f);
        System.out.println("X.a: " + xx2. a);
        System.out.println("-----------------------");
        X xy = new Y(7.5);
        System.out.println("X.a: " + ((X) xy).a);
        System.out.println("Y.a: " + ((Y) xy).a);
        System.out.println("-----------------------");
        Y yy = new Y(7);
        System.out.println("X.a: "+((X) yy).a);
        System.out.println("Y.a: "+((Y) yy).a);
        System.out.println("-----------------------");

        int i = 1;
        long lo = 2;
        System.out.print("(1)");
        xx1.f(i, xy);
        System.out.print("(2)");
        xx1.f(lo, xx1);
        System.out.print("(3)");
        xx1.f(lo, yy);
        System.out.print("(4)");
        yy.f(i, yy);
        System.out.print("(5)");
        yy.f(i, xy);
        System.out.print("(6)");
        yy.f(lo, yy);
        System.out.print("(7)");
        xy.f(i, xx1);
        System.out.print("(8)");
        xy.f(lo, yy);

        //xy.f(i, yy);
        System.out.println("----------------------");

    }
}
