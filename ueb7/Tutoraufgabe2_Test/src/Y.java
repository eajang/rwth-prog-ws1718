public class Y extends X{
    public float a = 42;

    public Y(double a) {
        this((float) a-1);
        System.out.println("Y(D)");
    }
    public Y(float a) {
        super(a);
        this.a = a;
        System.out.println("Y(F)");
    }

    public void f(int i, X o) {
        System.out.println("Y.f(IX)");
    }
    public void f(int lo, Y o) {
        System.out.println("Y.f(IY)");
    }
    public void f(long lo, X o) {
        System.out.println("Y.f(LX)");
    }
    public void f(long lo, Y o) { System.out.println("Y.f(LY)");}
}
