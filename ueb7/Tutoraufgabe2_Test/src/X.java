public class X {
    public int a = 23;

    public X (int a) {
        super();
        this.a = a;
        System.out.println("X(I)");
    }

    public X (float x) {
        this((int) x + 1 );
        System.out.println("X(F)");
    }

    public void f(int i, X o) {
        System.out.println("X.f(IX)");
    }


    public void f(long lo, Y o) {
        System.out.println("X.f(LY)");
    }

    public void f(long lo, X o) {
        System.out.println("X.f(LX)");
    }


}
